<?php return [
    'plugin' => [
        'name' => 'Badges',
        'description' => 'User-awarded badges',
    ],
    'badge' => [
        'name' => 'Badge Name',
        'description' => 'Badge Description',
        'image' => 'Image',
    ],
    'common' => [
        'created' => 'Created',
        'updated' => 'Updated',
        'deleted' => 'Deleted On',
    ],
];